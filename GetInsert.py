#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from bs4 import BeautifulSoup
from urllib.request import urlopen
from dateutil.parser import parse
import unidecode
import mysql.connector as mysql


def GetProteinFromPDB(protein, force, velocity, doi_pulling ):

    
    
    
    url = "https://www.rcsb.org/structure/"+protein
    
    url_annotation= "https://www.rcsb.org/annotations/"+protein
    page_annotation = urlopen(url_annotation)
    html_annotation = page_annotation.read().decode("utf-8")
        #print(html)
    soup_structure = BeautifulSoup(html_annotation, "html.parser")
        #SCOP annotation 
    structure= soup_structure.find("a", {"class": "querySearchLink" }).text.strip()
    
    
    page = urlopen(url)
    html = page.read().decode("utf-8")
        #print(html)
    soup = BeautifulSoup(html, "html.parser")
    name= soup.title.text.split('-')[-1]
    pdb_id= soup.find("span", {"id": "structureID" }).text.strip()
    
    
        # =============================================================================
        # GET DOI
        # =============================================================================
        # recuperer le bloc html via l ' id
    brut_doi= soup.find("li", {"id": "header_doi"})
    
        # recuperer l'attribut href contenant le lien du doi
    doi= brut_doi.findChild("a")['href']
        
        # =============================================================================
        # Get classification
        # =============================================================================
        # recuperer le bloc par id 
    brut_classification= soup.find("li", {"id": "header_classification" }).text.strip()
    classification= str(brut_classification.split(':')[-1])
    
    
        # =============================================================================
        # Get organism
        # =============================================================================
    brut_organism= soup.find("li", {"id": "header_organism" }).text.strip()
    organism = str(brut_organism.split(':')[-1])
        
        # =============================================================================
        # mutation
        # =============================================================================
    brut_mutation= soup.find("li", {"id": "header_mutation" }).text.strip()
    mutation = str(brut_mutation.split(':')[-1])
                
        # =============================================================================
        # Deposited
        # =============================================================================
    brut_deposition= soup.find("li", {"id": "header_deposited-released-dates" }).text.strip()
    deposition_str = str(brut_deposition.split()[1])
    dt = parse(deposition_str)
    deposition= dt.date()
        
        # =============================================================================
        # Released
        # =============================================================================
    brut_released= soup.find("li", {"id": "header_deposited-released-dates" }).text.strip()
    released_str = str(brut_released.split()[-1])
    dt = parse(released_str)
    released= dt.date()
        
        # =============================================================================
        # authors 
        # =============================================================================
    brut_authors= soup.find("li", {"id": "header_deposition-authors" }).text.strip()
    authors = str(brut_authors.split(':')[-1])
        
        #pour eviter caractère 
    classification= unidecode.unidecode(classification)
    organism= unidecode.unidecode(organism)
    authors= unidecode.unidecode(authors)
    mutation= unidecode.unidecode(mutation)
    print(name, pdb_id, doi, classification, organism, mutation, deposition, released, authors, structure)
    
    
    try:
        connection = mysql.connect(host='localhost',
                                             database='name',
                                             user='username',
                                             password='password')
        
        if connection.is_connected():
            db_Info = connection.get_server_info()
            print("Connected to MySQL Server version ", db_Info)
    
    except mysql.Error as e:
        print("Error while connecting to MySQL", e)
    
    
    
    
    values= (  pdb_id, doi, classification, organism, mutation, deposition, released, authors, structure,  force,velocity,  doi_pulling)
    columns= "pdb_id , doi, classification ,organism ,  mutations, deposited, released, deposition_authors, structure, unfolding_force, velocity, doi_pulling "
    
    
    val= "".join( "%s, " *len(values))
    val= val[0: len(val)-2]
    query = "INSERT INTO AllProteins ("+ columns +") VALUES("+val+");" 
    try:
        cursor = connection.cursor()
        cursor.execute(query, values)
        connection.commit()
        cursor.close() 
        
    except mysql.Error as e:
        print('Error:', e)
    

import sys
if __name__ == "__main__":
    protein = sys.argv[1]
    velocity = sys.argv[3]
    force = sys.argv[2]
    doi_pulling = sys.argv[4]


    print(GetProteinFromPDB(protein,   force, velocity, doi_pulling))
