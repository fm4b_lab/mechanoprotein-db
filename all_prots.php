<!DOCTYPE html>
<html lang="en">
<head>

     <title> MechanoPro-DB</title>

     <meta charset="UTF-8">
   
  
      <link rel="stylesheet" href="css/data_style.css">
	  <link rel="https://cdn.datatables.net/buttons/3.0.0/css/buttons.dataTables.min.css">


	  <link rel="stylesheet" href="https://cdn.datatables.net/2.0.1/css/dataTables.dataTables.min.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/owl.carousel.css">
      <link rel="stylesheet" href="css/owl.theme.default.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/searchbuilder/1.7.0/css/searchBuilder.dataTables.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/datetime/1.5.2/css/dataTables.dateTime.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css">
      <link href="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

     <!-- SCRIPTS -->

 




	<script type='text/javascript'  language='javascript'  src="https://code.jquery.com/jquery-3.5.1.js"></script>


	<script type='text/javascript' language='javascript'  src="https://cdn.datatables.net/2.0.1/js/dataTables.min.js"> </script>

	<script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"> </script>
	<script  src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"> </script>
	<script  src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>


	<script src="https://cdn.datatables.net/buttons/3.0.0/js/dataTables.buttons.min.js"> </script>
	<script  src="https://cdn.datatables.net/buttons/3.0.0/js/buttons.colVis.min.js"> </script>
	<script  src="https://cdn.datatables.net/buttons/3.0.0/js/buttons.html5.min.js"> </script>
	
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.js"></script>
    <script src="https://cdn.datatables.net/searchbuilder/1.7.0/js/dataTables.searchBuilder.js"></script>

        
    <script src="https://cdn.datatables.net/datetime/1.5.2/js/dataTables.dateTime.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>







</head>
<body id="top">


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MechanoPro-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li class="active"><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section>
          <div class="container">
               <div class="text-center">
                    <h1>MechanoProtein DataBase (MechanoPro-DB)</h1>

                    <br>

                    <p class="lead"> Proteins from Force Spectroscopy Experiments and Molecular Dynamics Simulations</p>
               </div>
          </div>
     </section>

  <section class="section-background"   > 
          <!-- DB Content here -->
<table   class="table" id="myTable"  style="width:50%">
        <thead>
            <tr align="center">
                <th >Name (Click for details)</th>             <!--0 -->

                <th>PDB ID</th>    <!--1 -->
            
                <th>SCOP annotation</th>
                <th > Highest unfolding forces/ Clamp forces [pN]</th>
                <th >Standard Deviation of force [pN]</th>

            
            
                <th >Velocity [nm/s]</th>
                <th >Loading Rate [pN/s]</th>
            
                
            	<th > Xu [nm]</th>
                <th > ΔG [kBT]</th>

                <th > Contour Length [nm]</th>
                <th > Koff [s-¹]</th>

                <th > Temperature (°C )</th>
                <th > Experimental Conditions</th>


                <th > Total Length (AA)</th>
                <th > Domain Coords</th>
                <th> 3D Structure (click on the protein)</th>
                
              <!--  <th> Topology</th> -->




                <th >Organism</th>
                <th >Mutations</th>
                <th >Classification</th>

                <th >Technique</th>
                <th > Pulling Mode </th>


                <th >DOI Structure</th>

                <th >Released</th>
                <th>Authors</th>
                <th > Pulling Reference</th>
                <th >Unfolding Annotation</th>


                <th >Unfolding Pathway</th>
                <th > Mechanical Clamp </th>
                <th > Unfolding Rate at Clamp Force [s-¹]</th>
                <th > F05 [pN]</th>







            </tr>    
               </thead>


	<tbody id="myTableBody" >
    <?php 
    	//session_start();
    	include 'DB_cnx.php';

        $sql ="SELECT  id, pdb_id, name, pulling_mode, unfolding_rate_maxforce, FO5, UF1, vel1, clamp_motif, uniprot,  classification, velocity, organism, doi, loading_rate, unfolding_force, mutations, structure, released, deposition_authors, doi_pulling, unfolding_annotation,  sd_force, protein_length, domain_coords, technique, Xu, contour_length, koff,  pathway, DeltaG, other_forces, other_vel, other_lr, other_sd_force, T, experimental_conditions, author_pulling, year_pulling FROM MPDB_Proteins ORDER BY Xu" ; 
          
    
    
         foreach  ($db->query($sql) as $row) { 
                echo "<tr>";
         
         
                print "<td > <a href = details.php?pdb_id=" .$row['pdb_id']."&id=". $row['id'] . "&name=".urlencode($row['name'])."&force=".$row['unfolding_force']."&velocity=".$row['velocity']."&year=".$row['year_pulling']."&author=".urlencode($row['author_pulling']) . "&doi=".$row['doi_pulling'] ." target='_blank'> ".  $row['name'] . " </td>";

				
         		if (is_null($row['pdb_id']))
                {
                	$prot_id= $row['uniprot'];
                    print "<td  ><a href=https://www.uniprot.org/uniprotkb/".$prot_id ."/entry  target='_blank'> uniprotID: ".$prot_id ."  </a> </td>";
                	print "<td  ><a href=http://scop.mrc-lmb.cam.ac.uk/search?t=txt;q=".  $prot_id . "  target='_blank'>  "   .$row['structure'] ." </a> </td>";

                	
                
                }
         		else
                {	$pdb_id= $row['pdb_id'];
                    print "<td  ><a href=https://www.rcsb.org/structure/".$pdb_id ." target='_blank'> ".$pdb_id ."  </a> </td>";
                	print "<td> <a href=https://scop.berkeley.edu/pdb/code=".$pdb_id.    " target='_blank'> "    .$row['structure'] . "</td>";

                }
         
           

                
         
  
         		if (is_null($row['UF1']))
                	{
                 		print "<td >".$row['unfolding_force']. "</td>";
                	}
         		else
               	 	{
                		print "<td > <a href = moreForces.php?pdb_id=" .$row['pdb_id']."&id=". $row['id'] . "&name=".urlencode($row['name'])."&force=".$row['unfolding_force']."&velocity=".$row['velocity']."&year=".$row['year_pulling']."&author=".urlencode($row['author_pulling']) . "&doi=".$row['doi_pulling'] ." target='_blank'> ".  $row['unfolding_force'] . " </td>";
                	}


        //  		print "</td>";
         
         
                print "<td>".$row['sd_force'] .  "<br>" ;   
         
                $other_sd= $row['other_sd_force'];

         		if (strlen($other_sd) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_sd= explode( ', ', $other_sd );
         		$num_sd=0;
         		foreach($all_sd as $sd) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_sd++;
                	print intval($sd);
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                }
                
         print "</details>";
                }
         
          print "</td>";
         		

         
         //velocity
           //     print "<td>".$row['velocity'] . "<br>" ;   
                  		if (is_null($row['vel1']))
                {
                 print "<td >".  preg_replace('/(e[+-])(\d)$/', '${1}0$2', sprintf('%1.2e',$row['velocity']))."</td>";
                }
         		else
                {
                print "<td > <a href = moreForces.php?pdb_id=" .$row['pdb_id']."&id=". $row['id'] . "&name=".urlencode($row['name'])."&force=".$row['unfolding_force']."&velocity=".$row['velocity']."&year=".$row['year_pulling']."&author=".urlencode($row['author_pulling']) . "&doi=".$row['doi_pulling'] ." target='_blank'> ".  $row['velocity'] . " </td>";
                }

    

         
         // LOADING RATE
         
                print "<td>". ($row['loading_rate']) . "<br>" ;   
         
                $other_lr= $row['other_lr'];

         		if (strlen($other_lr) != 0){
         		print "<details>";
  				print "<summary> See more</summary>";
				
         		$all_lr= explode( ', ', $other_lr );
         		$num_lr=0;
         		foreach($all_lr as $lr) 
               
                {
                	
                	print "<table>";
                	print "<td>";
                	$num_lr++;
                	print $lr;
                	print "<br>";
                    
					print "</td>";
                	print "</table>";
                }
                
         print "</details>";
                }
         
          print "</td>";
         		
         
                
         		//print "<td>". ($row['Xu']) . "</td>";
                print "<td>". (isset($row['Xu']) ? $row['Xu'] : 'N/A'). "</td>";

                
         	//	print "<td>". ($row['DeltaG']) . "</td>";
                print "<td>". (isset($row['DeltaG']) ? $row['DeltaG'] : 'N/A'). "</td>";

         
                print "<td >".$row['contour_length'] . "</td>";
         
         
                //print "<td>".($row['koff'] ) . "</td>";
                print "<td>". (isset($row['koff']) ? $row['koff'] : 'N/A'). "</td>";

                
         		print "<td>". (isset($row['T']) ? $row['T'] : 'N/A'). "</td>";
                print "<td >". (isset($row['experimental_conditions']) ? $row['experimental_conditions'] : 'N/A'). "</td>";

 //style='font-size: 11px;'

                print "<td>".$row['protein_length'] . "</td>";
                print "<td>".$row['domain_coords'] . "</td>";

				if (is_null($row['pdb_id']))
                {
                	print "<td > <a href = structure.php?id=" .$row['pdb_id']. "  target='_blank'><img src=ProteinSnapshots/NA.jpg width=120 height=120 ></a></td>";

                	
                }
         		else
                {
         			print "<td > <a href = structure.php?id=" .$row['pdb_id']. "  target='_blank'><img src=ProteinSnapshots/" .$row['pdb_id'] . ".jpg width=120 height=120 ></a></td>";
                }
  
                print "<td>".$row['organism'] . "</td>";
                print "<td>".$row['mutations'] . "</td>";
                print "<td>".$row['classification'] . "</td>";
                
         		print "<td>".$row['technique'] . "</td>";
         		print "<td>" .  $row['pulling_mode'] . "</td>";

         
         
         		print "<td><a href=".$row['doi']."  target='_blank'>  DOI </a> </td>";
         
                print "<td>".$row['released'] . "</td>";
                print "<td>".$row['deposition_authors'] . "</td>";
         	

         
         // DISPALY SNAPSHOT INSTEAD
         
         		$doi_pulling= $row['doi_pulling'];

               print "<td class='author'><a href=".$doi_pulling."  target='_blank'>   ".$row['author_pulling']." - ".$row['year_pulling']." </a>   </td>";


         		//annotation DOI
				print  "<td width='1px' >" . ($row['unfolding_annotation'] ) . "</td>";
         
  
      
         		print "<td>" .  $row['pathway'] . "</td>";

         
         
        		print "<td> <a href=http://ptgl.uni-frankfurt.de/results.php?q=".strtolower($row['pdb_id'])."A target='_blank'> <img id=currentPhoto src=topology/". $row['clamp_motif']  .".png onerror=\"this.src='topology/NA.png'\" width=80 height=80 /> </a> </td>";

                print "<td>" .  $row['unfolding_rate_maxforce'] . "</td>";
                print "<td>" .  $row['FO5'] . "</td>";


                print "</tr>";

			}

        ?>
               </tbody>
        <tfoot> 

        </tfoot>

    </table>



<script>

//FONTIONNEL :  COLLECTION (show and hide) and SEARCHING BUILDER
        $(document).ready(function () {
        
        var table=    $('#myTable').DataTable({

             //   columnDefs: [{ type: 'office', targets: 2 }],
                dom: 'BQlfrtip',
                pageLength: 5,

                buttons: ['colvis', 'csv', 'excel'],
                columnDefs: [{ 
                			"targets": [16, 18, 17, 21, 13, 6, 4, 23, 22, 8, 11, 28, 29], 
                			"visible": false
                    //  className: 'dt-body-center'

                
                
                			}], // Initially hide columns,
                searchBuilder: {
                
 
                	
                	enableRegex: true,
				
                }
            });
        // Change the position of the search box to the footer

        });


     
	</script>


      
     </section>

     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Université Aix Marseille </p>
                              </div>
                         </div>
                    </div>

                      <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

    


</body>
<style>
details {
  font: 12px "Open Sans", Calibri, sans-serif;
}

details > summary {
  background-color: #ddd;
  border: none;
  box-shadow: 3px 3px 4px black;
  cursor: pointer;
}
details[open] > summary {
  background-color: #ccf;
}


</style>
</html>