<?php
 
    include 'DB_cnx.php';



if($_SERVER['REQUEST_METHOD'] === 'POST')

//   if(isset($_POST["Submit"])) 

    {

    	$pdb_id = $_POST["pdb_id"];
    	$force = $_POST["force"];

    	//$force1 = $_POST["force_0"];
    	//$force2 = $_POST["force_1"];
    	//$force3 = $_POST["force_2"];


    	//$velocity1 = $_POST["velocity_1"];
    //	$velocity2 = $_POST["velocity_2"];
    //	$velocity3 = $_POST["velocity_3"];


    	$velocity = $_POST["velocity"];
		
    	$mutation = $_POST["forMutation"];
    	$structure = $_POST["FormStructure"];
    	$organism = $_POST["organism"];
    	$name = $_POST["name"];
    	$common_name = $_POST["common_name"];
    	$doi_pulling = $_POST["doi_pulling"];
    	$classification = $_POST["classification"];
    	$mail = $_POST["mail"];
    	$comment = $_POST["comment"];

        $input = $_POST["pathway"];
        $pattern = '/^N((_(I\[([0-9]+)\])){1,10})?_U\[([0-9]+)\]$/';
		
		if(!isset($_POST["loading_rate"]) || trim($_POST["loading_rate"]) == '')
		{
   			//echo "You did not fill out the loading rate.";
        	$loading_rate=null;
		}
		else
        {
        	$loading_rate= $_POST["loading_rate"];
        }

    
		//writing the info in a text file instead of inserting in DB
		$data = 'pdb id : '. $pdb_id . ' \n  force: ' . $force ."   velocity: ".$velocity. "    loading rate: ". $loading_rate." \n protein exact name: ". $name."  \n  protein common name: ". $name."   \n   doi_pulling: ". $doi_pulling. "  \n   classification: ". $classification. "  \n  email: ". $mail . " \n  suggested pathway: ". $input. " \n  comments: ". $comment ;
    	$filename = 'uploads/'. date('YmdHis').".txt";
    	$ret = file_put_contents($filename, $data, FILE_APPEND | LOCK_EX);
    	if($ret === false) {
        		die('There was an error writing this file');
    	}
    	else {
        		echo "<p class='lead'> information saved properly <p/>";
    	}
	 



//		if(isset($_POST['pb']))    
  //      {


            //	$command= "python3 GetInsert.py $pdb_id $force $velocity $doi_pulling 2>&1"  ;
          	 // 	$output= shell_exec($command);
            	//print $output;

           	 	if (preg_match($pattern, $input)) {
                	echo "<p class='lead'> Valid unfolding pathway: $input </p>";
            		} 	
            	else {
                    echo "<p class='lead'> Empty or Invalid unfolding pathway $input </p>";  
           		 }
 
 			
 	//	}

    
    	//else {
    	/*    
    
    		$sql= "INSERT INTO contributions (name , classification, organism, mutations, doi_pulling, velocity, loading_rate, unfolding_force, structure, pathway) VALUES ('$name', '$classification', '$organism', '$mutation', '$doi_pulling', '$velocity', '$loading_rate', '$force', '$structure', '$pathway' )";
    		//print($sql);
    		$db->query($sql);
			$statement = $db->prepare($sql);
 			$statement->execute();
            
            */
        /*
                if (preg_match($pattern, $input)) {
            echo "<p class='lead'> Valid unfolding pathway: $input </p>";
        } else {
            echo " <p class='lead'> Empty or Invalid unfolding pathway: $input </p>";
        }
        */
    
    		//	}
    
   
    
    	//print $force;
			//$output= shell_exec("python3 GetInsert.py $pdb_id $velocity $loading_rate $force $doi_pulling");

//        $name= "name: ".$name." + common name: ".$common_name." pdb_id: ".$pdb_id;
  //      $query= "INSERT INTO Users (protein_name ,  doi_pulling, mail, comments) VALUES ('$name',  '$doi_pulling',  '$mail' , '$comment' )";
    //	$db->query($query);
	//	$state = $db->prepare($query);
 	//	$state->execute();



    //Mail sending function
		$subject = "Thank you ! [submission]";
		$to = $mail." ,  mesbah.ismahene@gmail.com";
		$from = "Ismahene MESBAH";

		//data
		$msg = '
     	<html>
      		<head>
       			<title> Thank You !'.$mail.' </title>
      		</head>
      	<body>
        


        		<h2 class="titre" style="text-align:center;"> Mechanoprotein DataBase </h1>
		
        	<p  style="text-align:center;" > Dear user: '.$mail.' </p>

       		<p  style="text-align:center;" > Your protein has been submitted successfully ! </p>
            <br>
             <p  style="text-align:center;" > Thank you for your contribution, we will display the protein soon in our website. </p>
			<br>

       	<center>
        <table id="protein">
        	<tr>
         	<th> Protein </th>
            <th> PDB ID</th>
            <th> Force [pN]</th>
            <th> Loading Rate [pN/s]</th>
            <th> velocity [nm/s]</th>
             <th> DOI </th>



        	</tr>
        	<tr>
         	<td>  '.$_POST["name"].' </td> 
            <td> '.$pdb_id.' </td>
            <td> '.$force.'  </td>
            <td> '.$loading_rate. '  </td>
            <td> '.$velocity.'  </td>
            <td> '.$doi_pulling.'</td>
        	</tr>
            '.
        /*
            <tr>
            <td></td> 
            <td>' .$pdb_id.' </td> 
            <td>'.$force1. '</td> 
            <td>' . $loading_rate1 .'</td>
            <td>'.$velocity1. '</td> 
            <td></td> 
            </tr>
            
            <tr>
            <td></td> 
            <td>' .$pdb_id.' </td> 
            <td>'.$force2. '</td> 
            <td>' . $loading_rate2 .'</td>
            <td>'.$velocity2. '</td> 
            <td></td> 
            </tr>
            
            <tr>
            <td></td> 
            <td>' .$pdb_id.' </td> 
            <td>'.$force3. '</td> 
            <td>' . $loading_rate3 .'</td>
            <td>'.$velocity3. '</td> 
            <td></td> 
            </tr>
            */
        '
            
       </table>
       </center>
      </body>
		<br>
      <footer><center> &copy; Copyright 2021 Aix-Marseille University</center></footer>
     </html>
     <style>
     .title
		{
			font-weight: 600;
			position: center;  
			font-size: 36px;
			line-height: 200px;
			padding: 15px 15px 15px 15%;
			color:  black;
			box-shadow: 
				inset 0 0 0 1px rgba(22, 160, 133, 1), 
				inset 0 0 5px rgba(53,86,129, 0.4),
				inset -285px 0 35px white;
				border-radius: 0 10px 0 10px;
			background: #fff url(https://drive.google.com/file/d/1I03NiqQJm0vK-TuHsE2Ml0p0OvtWGDrk/view?usp=sharing) no-repeat center;
		}
	.titre {
		font-weight: normal;
		position: center;
		text-shadow: 0 -1px rgba(0,0,0,0.6);
		font-size: 28px;
		line-height: 40px;
		background: #355681;
		border: 1px solid #fff;
		padding: 5px 15px;
		color: white;
		border-radius: 0 10px 0 10px;
		box-shadow: inset 0 0 5px rgba(53,86,129, 0.5);
		}
        #protein {
  			font-family: Arial, Helvetica, sans-serif;
  			border-collapse: collapse;
  			width: 100%;
			}

		#protein td, #protein th {
  		border: 1px solid #ddd;
  		padding: 8px;
		}

		#protein tr:nth-child(even){background-color: #f2f2f2;}

		#protein tr:hover {background-color: #ddd;}

	#protein th {
  		padding-top: 12px;
  		padding-bottom: 12px;
  		text-align: left;
  		background-color: #355681;
  		color: white;
		}
     </style>
     ';

		//Headers
	
    	$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= "From: <".$from. ">" ;
// In case any of our lines are larger than 70 characters, we should use wordwrap()
		//$message = wordwrap($message, 70, "\r\n");
		mail($to,$subject,$msg,$headers);
		//echo "Mail Sent.";
    
    
    }
?>
<?php
/*
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["the_file"]["name"]);
$uploadOk = 1;
$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

// Check if image file is a actual image or fake image
if(isset($_POST["Start Upload"])) {
 
  $check = getimagesize($_FILES["the_file"]["tmp_name"]);
  if($check !== false) {
    echo "File is OK - " . $check["mime"] . ".";
    $uploadOk = 1;
  } else {
    echo "File is not OK.";
    $uploadOk = 0;
  }
}


// Check if file already exists
if (file_exists($target_file)) {
  echo "Sorry, file already exists.";
  $uploadOk = 0;
}

// Check file size
if ($_FILES["the_file"]["size"] > 500000) {
  echo "Sorry, your file is too large.";
  $uploadOk = 0;
}

// Allow certain file formats
if($FileType != "csv" && $FileType != "txt" && $FileType != "dat"
&& $FileType != "xls" ) {
  echo "Sorry, only CSV , DAT , TXT & XLS files are allowed.";
  $uploadOk = 0;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
  echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
  if (move_uploaded_file($_FILES["the_file"]["tmp_name"], $target_file)) {
    echo "The file ". htmlspecialchars( basename( $_FILES["the_file"]["name"])). " has been uploaded.";
  } else {
    echo "Sorry, there was an error uploading your file.";
  }
}
*/
?>

<!DOCTYPE html>
<html>
<head>
<title> [Submission] MP-DB</title>

     <meta charset="UTF-8">
   
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">


     <!-- SCRIPTS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>






     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/data_style.css">

</head>

<body id="top">

     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MP-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li class="active"><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section>
          <div class="container">
               <div class="text-center" >
                    <h1>MechanoProtein DataBase  </h1>
 
                    <br>

                    <p class="lead"> Proteins From Force Spectroscopy Experiments and Steered Molecular Dynamics </p>
               </div>
          </div>
     </section>

     <section class="section-background"> 

              <div class="container">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                              <div class="text-center">

                              		<h1>Thank you for your contribution </h1>
  										<p class="lead">Your protein has been submitted. 
                              			</p>
  										<p>You should be receiving an automated email right about *now* to confirm your protein sent. 
                                    	Then your protein will appear on our website within 2-3 days!
                              			</p>
                              	<div>
                         			<div class="contact-image" style="display: grid; grid-template-columns: 1fr 1fr; gap: 60px;justify-items: center; align-items: center;">
                         			<!--	<img src="images/lai-logo.png" class="img-responsive"   style="padding:0px 10px; border-right: 1px  #000000;">   -->
                         				<img src="images/logo-CENTURI.png" class="img-responsive"height="6" style="padding: 0px 10px; border-right: 0.5px  #000000;">
                         				<img src="images/Logo_AMU.png" class="img-responsive"  style="padding: 0px 2px; border-left: 1px  #000000;">
                          				<img src="images/logo-ibdm.png" class="img-responsive" style="padding-left: 1px  #000000;">
                          				<img src="images/Inserm-logo.png" class="img-responsive"  style="padding-right: 0px 10px; ">
                     				</div>
                 				</div>

                              
                              </div>
                         </div>
                    </div>
              </div>
    
                      
       </section>
		</body>
    <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Université Aix Marseille </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>
  

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>


  	</html>