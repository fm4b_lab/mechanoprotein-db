<!DOCTYPE html>
<html>
<head>
<?php


    $id_pdb=$_GET['pdb_id'];
    $id=$_GET['id'];

	$force=$_GET['force'];
	$name=urldecode($_GET['name']);
	$year=$_GET['year'];
	$velocity=$_GET['velocity'];
	$author=$_GET['author'];
	$doi= $_GET['doi'];



	//echo htmlspecialchars($id);


?>
     <title> MP-DB</title>

     <meta charset="UTF-8">
   
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">


     <!-- SCRIPTS -->


     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>




     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/data_style.css">

</head>
 <script src='https://cdn.plot.ly/plotly-2.18.0.min.js'></script>                 

<body id="top">


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MP-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section>
          <div class="container">
               <div class="text-center">
                    <h1>MechanoProtein DataBase (MP-DB)</h1>

                    <br>

                    <p class="lead"> Proteins from Force Spectroscopy Experiments and Steered Molecular Dynamics Simulations</p>
               </div>
          </div>
     </section>

     <section class="section-background"> 
        <table  id="myTable" class="table" >    
     	<thead>
     		<td> Force [pN] </td>
          	<td> Velocity [nm/s] </td>
          	<td> Loading Rate [pN/s] </td>

     	</thead>
              <div class="container">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                              <div class="text-center">
           						 <?php echo " <h2  style='text-align:center;'>  " . $name. "  </h2>" ?>
                              	
                                	<?php 	
                                        	include 'DB_cnx.php';


        $sql ="SELECT UF1, UF2, UF3, UF4, UF5, UF6, UF7, UF8, UF9, UF10, vel1, vel2, vel3, vel4, vel5, vel6, vel7, vel8, vel9, vel10, lr1, lr2, lr3, lr4, lr5, lr6, lr7, lr8, lr9, lr10 FROM MPDB_Proteins WHERE id='".$id."' ;" ; 
    
    
	foreach ($db->query($sql) as $row) { 
    	print "<tr>";
    	print "<td>". $row['UF1']. "</td>";
    	print "<td>". $row['vel1']. "</td>";
        print "<td>". $row['lr1']. "</td>";

    	print "</tr>";
    
   		print "<tr>";
    	print "<td>". $row['UF2']. "</td>";
    	print "<td>". $row['vel2']. "</td>";
    	print "<td>". $row['lr2']. "</td>";
    	print "</tr>";
    
    	print "<tr>";
   	 	print "<td>". $row['UF3']. "</td>";
    	print "<td>". $row['vel3']. "</td>";
    	print "<td>". $row['lr3']. "</td>";
    	print "</tr>";
    
    	print "<tr>";
    	print "<td>". $row['UF4']. "</td>";
    	print "<td>". $row['vel4']. "</td>";
    	print "<td>". $row['lr4']. "</td>";
    	print "</tr>";
    
        	print "<tr>";
    	print "<td>". $row['UF5']. "</td>";
    	print "<td>". $row['vel5']. "</td>";
    	print "<td>". $row['lr5']. "</td>";
    	print "</tr>";
    
        	print "<tr>";
    	print "<td>". $row['UF6']. "</td>";
    	print "<td>". $row['vel6']. "</td>";
    	print "<td>". $row['lr6']. "</td>";
    	print "</tr>";
    
        	print "<tr>";
    	print "<td>". $row['UF7']. "</td>";
    	print "<td>". $row['vel7']. "</td>";
    	print "<td>". $row['lr7']. "</td>";
    	print "</tr>";
    
    
        	print "<tr>";
    	print "<td>". $row['UF8']. "</td>";
    	print "<td>". $row['vel8']. "</td>";
    	print "<td>". $row['lr8']. "</td>";
    	print "</tr>";
    
        	print "<tr>";
    	print "<td>". $row['UF9']. "</td>";
    	print "<td>". $row['vel9']. "</td>";
    	print "<td>". $row['lr9']. "</td>";
    	print "</tr>";
    
        	print "<tr>";
    	print "<td>". $row['UF10']. "</td>";
    	print "<td>". $row['vel10']. "</td>";
    	print "<td>". $row['lr10']. "</td>";
    	print "</tr>";
    
	}

         

                 ?>
                              </div>
                         </div>
                    </div>
              </div>
    
     
	<?php // header('Access-Control-Allow-Origin: *'); ?>
     </table>
                 
    <div id="plot"></div>
    <select id="plotOption">
        <option value="velocity">Velocity (nm/s)</option>
        <option value="loading_rate">Loading Rate (pN/s)</option>
    </select>
    <script>

          // JavaScript code to generate the Plotly plot
          var forces = [<?php echo $row['UF1']; ?>, <?php echo $row['UF2']; ?>, <?php echo $row['UF3']; ?>, <?php echo $row['UF4']; ?>,  <?php echo $row['UF5']; ?>,  <?php echo $row['UF6']; ?>,  <?php echo $row['UF7']; ?>,  <?php echo $row['UF8']; ?>,  <?php echo $row['UF9']; ?>,  <?php echo $row['UF10']; ?>];
          var velocities = [<?php echo $row['vel1']; ?>, <?php echo $row['vel2']; ?>, <?php echo $row['vel3']; ?>, <?php echo $row['vel4']; ?>, <?php echo $row['vel5']; ?>, <?php echo $row['vel6']; ?>, <?php echo $row['vel7']; ?>, <?php echo $row['vel8']; ?>, <?php echo $row['vel9']; ?>, <?php echo $row['vel10']; ?>];
    	  var lrValues = [<?php echo $row['lr1']; ?>, <?php echo $row['lr2']; ?>, <?php echo $row['lr3']; ?>, <?php echo $row['lr4']; ?>, <?php echo $row['lr5']; ?>, <?php echo $row['lr6']; ?>, <?php echo $row['lr7']; ?>, <?php echo $row['lr8']; ?>, <?php echo $row['lr9']; ?>, <?php echo $row['lr10']; ?>];
          var trace = {
               x: velocities,
               y: forces,
               mode: 'markers',
               type: 'scatter',
               marker: {
                    color: 'rgb(17, 157, 255)',
                    size: 10
               }
          };

          var layout = {
               title: 'Graphical Representation of the table: Force versus Velocity',

               xaxis: {
                    title: 'Velocity [nm/s]',
                              type: 'log',

               },
               yaxis: {
                    title: 'Force [pN]'
               }
          };

          var data = [trace];

          Plotly.newPlot('plot', data, layout);




		document.getElementById('plotOption').addEventListener('change', function () {
    		// Check if the element exists
    	if (!this) {
       	 	console.error("Dropdown menu not found.");
        	return;
    	}

    	// Check if the value attribute exists
    	var optionValue = this.value;
    		if (!optionValue) {
        		console.error("Value attribute not found on dropdown menu.");
        	return;
    	}


    	if (optionValue === 'velocity') {
        	var update= {
                    	 x:velocities,
        				//  'title': 'Velocity [nm/s]',
                          y: forces,
               				mode: 'markers',
               				type: 'scatter',
              			 marker: {
                   		 color: 'rgb(17, 157, 255)',
                  			  size: 10
               		}
            };
         
             var layout_update = {
               title: 'Graphical Representation of the table: Forces versus Velocity',

               xaxis: {
                    title: 'Velocity [nm/s]',
                              type: 'log',

               },
               yaxis: {
                    title: 'Force [pN]'
               }
          };

	  Plotly.newPlot('plot', [update],  layout_update);

    	} else if (optionValue === 'loading_rate') {
        	var update_lr= {
        		 x:lrValues,
        		//'title': 'Loading Rate [pN/s]',
                 y: forces,
               mode: 'markers',
               type: 'scatter',
               marker: {
                    color: 'red',
                    size: 10
               }
            }
            var layout_update = {
               title: 'Graphical Representation of the table: Force versus Loading Rate',

               xaxis: {
                    title: 'Loading Rate [pN/s]',
                              type: 'log',

               },
               yaxis: {
                    title: 'Force [pN]'
               }
          }
        Plotly.newPlot('plot', [update_lr],  layout_update);
    	}
   //    Plotly.newPlot('plot', data_update_lr,  layout_update);
        } );

    		// Update the plot according to the user's selection
    	
    //    Plotly.relayout('plot', update);


     </script>
	   
     </section>
</body>

     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Université Aix Marseille </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>
  

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

 <style>
 .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
}
 
</style>


</html>


