<!DOCTYPE html>
<html>
<head>
<?php


    $id_pdb=$_GET['pdb_id'];
    $id=$_GET['id'];

	$force=$_GET['force'];
	$name=urldecode($_GET['name']);
	$year=$_GET['year'];
	$velocity=$_GET['velocity'];
	$author=$_GET['author'];
	$doi= $_GET['doi'];



	//echo htmlspecialchars($id);


?>
     <title> MP-DB</title>

     <meta charset="UTF-8">
   
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="">
     <meta name="keywords" content="">
     <meta name="author" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">


     <!-- SCRIPTS -->


     <script src="js/jquery.js"></script>
     <script src="js/bootstrap.min.js"></script>
     <script src="js/owl.carousel.min.js"></script>
     <script src="js/smoothscroll.js"></script>
     <script src="js/custom.js"></script>






     <!-- MAIN CSS -->
     <link rel="stylesheet" href="css/data_style.css">

</head>

<body id="top">


     <!-- MENU -->
     <section class="navbar custom-navbar navbar-fixed-top" >
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="#" class="navbar-brand">MP-DB</a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-nav-first">
                         <li><a href="index.php">Home</a></li>
                         <li><a href="all_prots.php">Data</a></li>
                         <li><a href="about-us.html">About Us</a></li>
                         <li><a href="team.html">Authors</a></li>
                         <li><a href="InsertInfo.php">Contribute</a></li>
                    </ul>
               </div>

          </div>
     </section>

     <section>
          <div class="container">
               <div class="text-center">
                    <h1>MechanoProtein DataBase (MP-DB)</h1>

                    <br>

                    <p class="lead"> Proteins from Force Spectroscopy Experiments and Steered Molecular Dynamics Simulations</p>
               </div>
          </div>
     </section>

     <section class="section-background"> 
     
              <div class="container">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                              <div class="text-center">
           						 <?php echo " <h2  style='text-align:center;'>  " . $name. "  </h2>" ?>
                              	
                                	<?php 	
											print "<img   src=figures/".$id. ".png  class='center'  />" ;
                                        	include 'DB_cnx.php';

         
        $sql ="SELECT  figure_caption FROM MPDB_Proteins WHERE id='".$id."' ;" ; 
    
    
         foreach  ($db->query($sql) as $row) { 
         	print "<br>";
         	print "<div>";
         	print "Figure: "; 
         	print $row['figure_caption']; 
            print "<br>";
            print "Reference: ";
            print $author ." et al. ". $year;

            print "<a  href=". $doi ." style='color:blue;'> (".$doi. " )   </a>" ;


            print "</div>";

         
         }
                 ?>
                              </div>
                         </div>
                    </div>
              </div>
    
     
     
	<?php // header('Access-Control-Allow-Origin: *'); ?>

	   
     </section>
</body>

     <!-- FOOTER -->
     <footer id="footer">
          <div class="container">
               <div class="row">

                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              <div class="section-title">
                                   <h2>Address</h2>
                              </div>
                              <address>
                                   <p> 163 boulevard de Luminy <br> 13009 Marseille</p>
                              </address>


                              <div class="copyright-text"> 
                                   <p>Copyright &copy; 2021 Université Aix Marseille </p>
                              </div>
                         </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                         <div class="footer-info">
                              

                              <div class="footer_menu">
                                   <h2>Quick Links</h2>
                                   <ul>
                                        <li><a href="index.html">Home</a></li>
                                        <li><a href="about-us.html">About Us</a></li>
                                        <li><a href="terms.html">Terms & Conditions</a></li>
                                        <li><a href="contact.html">Contact Us</a></li>
                                   </ul>
                              </div>
                         </div>
                    </div>
  

                    <div class="col-md-4 col-sm-12">
                         <div class="footer-info newsletter-form">
                              <div class="section-title">
                                   <h2>Newsletter Signup</h2>
                              </div>
                              <div>
                                   <div class="form-group">
                                        <form action="#" method="get">
                                             <input type="email" class="form-control" placeholder="Enter your email" name="email" id="email" required>
                                             <input type="submit" class="form-control" name="submit" id="form-submit" value="Send me">
                                        </form>
                                   </div>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </footer>

 <style>
 .center {
  display: block;
  margin-left: auto;
  margin-right: auto;
  width: 70%;
}
 
</style>


</html>


